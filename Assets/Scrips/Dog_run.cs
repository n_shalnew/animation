﻿using UnityEngine;
using System.Collections;

public class Dog_run : MonoBehaviour {


    private Vector2 movement;
    private Rigidbody2D rb;
    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody2D>();
    }
	
	// Update is called once per frame
	void Update () {
        movement = new Vector2(-25, -30);
    }
    void FixedUpdate()
    {
        // применение движения
        rb.velocity = movement;
    }
}
