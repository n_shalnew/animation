﻿using UnityEngine;
using System.Collections;

public class Revert_trigger : MonoBehaviour
{

    public GameObject target;

    // Use this for initialization
    void Start()
    {

    }
    void OnTriggerExit2D(Collider2D one)
    {
        if (target != null)
        {
            Vector3 theScale = target.GetComponent<ForOrc>().transform.localScale;
            //            Vector3 theScale = transform.localScale;
            //зеркально отражаем персонажа по оси Х
            theScale.x *= -1;
            //задаем новый размер персонажа, равный старому, но зеркально отраженный
            target.GetComponent<ForOrc>().transform.localScale = theScale;
            if (target.GetComponent<ForOrc>().direction == 1)
                target.GetComponent<ForOrc>().direction = -1;
            else if (target.GetComponent<ForOrc>().direction == -1)
                target.GetComponent<ForOrc>().direction = 1;
            //Debug.Log("Exit from area! " + target.GetComponent<ForOrc>().direction);
        }
    }

}

