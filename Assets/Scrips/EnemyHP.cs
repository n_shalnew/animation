﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EnemyHP : MonoBehaviour
{
    public float Max_HP;
    public float HP;
    public GameObject Health_bar;

    float Max_transform;
    private Animator anim;
    // Use this for initialization
    private void Start()
    {
        anim = GetComponent<Animator>();
        HP = Max_HP;
        Max_transform = Health_bar.gameObject.transform.localScale.x;
        Health_bar.GetComponent<SpriteRenderer>().enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        /*!!!*/
        Health_bar.gameObject.transform.localScale = new Vector3((Max_transform*HP/Max_HP), Health_bar.gameObject.transform.localScale.y, Health_bar.gameObject.transform.localScale.z);
        if (HP < Max_HP && Health_bar.GetComponent<SpriteRenderer>().enabled == false)
            Health_bar.GetComponent<SpriteRenderer>().enabled = true;
        if (HP < 1)
        {
            Health_bar.gameObject.transform.localScale = new Vector3(0, 0, 0);
            anim.SetBool("Die", true);
        }

    }
    public void Deat_funct()
    {
        Destroy(gameObject, 0);
    }
}
