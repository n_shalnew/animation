﻿using UnityEngine;
using System.Collections;

public class CharacterControllerScript : MonoBehaviour
{

    /*------------*/
    public float Player_damage;
    public Transform punch1;
    public float punch1Radius;
    /******////
    //время перезарядки
    public float shootingRate = 0.25f;
    private float shootCooldown;//перезарядка
      

	/**** сенсорное управление*/
	public bool moveright;
	public bool moveleft;
	public bool canTouchStrike;
	/****/


    //переменная для установки макс. скорости персонажа
    public float maxSpeed = 10f;
    
	public float JumpForce;
	public bool IsGround;
	Rigidbody2D PlRb;
	SpriteRenderer PlSr;
	Animator MyAnim;

    /// <summary>
    /// Начальная инициализация
    /// </summary>
	private void Start()
    {
       
  
    }

    public void My_Attack()
    {
        if (CanAttack)
        {
            Fight2D.Action(punch1.position, punch1Radius, 9, Player_damage, false);
        }
    }

	public void Awake(){
		PlRb = GetComponent<Rigidbody2D> ();
		PlSr = GetComponent<SpriteRenderer> ();
		MyAnim = GetComponent<Animator> ();
	}

    /// <summary>
    /// Выполняем действия в методе FixedUpdate, т. к. в компоненте Animator персонажа
    /// выставлено значение Animate Physics = true и анимация синхронизируется с расчетами физики
    /// </summary>
  /*  private void FixedUpdate()
    {

        anim.SetBool("Strike", false);
        //определяем, на земле ли персонаж
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, groundRadius, whatIsGround);
        //устанавливаем соответствующую переменную в аниматоре
        anim.SetBool("Ground", isGrounded);
        //устанавливаем в аниматоре значение скорости взлета/падения
        anim.SetFloat("vSpeed", GetComponent<Rigidbody2D>().velocity.y);
        //если персонаж в прыжке - выход из метода, чтобы не выполнялись действия, связанные с бегом
        if (!isGrounded)
            return;
        //используем Input.GetAxis для оси Х. метод возвращает значение оси в пределах от -1 до 1.
        //при стандартных настройках проекта 
        //-1 возвращается при нажатии на клавиатуре стрелки влево (или клавиши А),
        //1 возвращается при нажатии на клавиатуре стрелки вправо (или клавиши D)
        float move = 0.0f;
        if (Input.GetKey(KeyCode.A))
            move = -1;
        if (Input.GetKey(KeyCode.D))
            move = 1;
        //float move = Input.GetAxis("Horizontal");
        if (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.Mouse0))
        {
            anim.SetBool("Strike", true);
            
        }
        //в компоненте анимаций изменяем значение параметра Speed на значение оси Х.
        //приэтом нам нужен модуль значения

        anim.SetFloat("Speed", Mathf.Abs(move));

        //обращаемся к компоненту персонажа RigidBody2D. задаем ему скорость по оси Х, 
        //равную значению оси Х умноженное на значение макс. скорости
        GetComponent<Rigidbody2D>().velocity = new Vector2(move * maxSpeed, GetComponent<Rigidbody2D>().velocity.y);

        //если нажали клавишу для перемещения вправо, а персонаж направлен влево
        if (move > 0 && !isFacingRight)
            //отражаем персонажа вправо
            Flip();
        //обратная ситуация. отражаем персонажа влево
        else if (move < 0 && isFacingRight)
            Flip();



		if (shootCooldown > 0)
		{
			shootCooldown -= Time.deltaTime;
		}
		//если персонаж на земле и нажат пробел...
		if (isGrounded && Input.GetKeyDown(KeyCode.Space))
		{
			//устанавливаем в аниматоре переменную в false
			anim.SetBool("Ground", false);
			//прикладываем силу вверх, чтобы персонаж подпрыгнул
			GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 600));
		}


    }*/
    public bool CanAttack
    {
        get
        {
            return shootCooldown <= 0f;
        }
    }
    private void Update()
    {
		MyAnim.SetBool("IsStrike", false);
		if (Input.GetAxisRaw ("Horizontal") > 0 || moveright) {
			PlRb.velocity = new Vector2 (maxSpeed, PlRb.velocity.y);
			PlSr.flipX = false;
		}
		else if (Input.GetAxisRaw ("Horizontal") < 0 || moveleft) {
			PlRb.velocity = new Vector2 (-maxSpeed, PlRb.velocity.y);
			PlSr.flipX = true;
		} 
		else {
			PlRb.velocity = new Vector2 (0, PlRb.velocity.y);
		}
		if (Input.GetButtonDown ("Jump") && IsGround) {
			PlRb.velocity = new Vector2 (PlRb.velocity.x, JumpForce);
		}

		if (shootCooldown > 0)
		{
			shootCooldown -= Time.deltaTime;
		}

		if (Input.GetKey(KeyCode.LeftControl) || canTouchStrike)
		{
			
				MyAnim.SetBool ("IsStrike", true);
				
		}

		MyAnim.SetBool ("IsGround",IsGround);
		MyAnim.SetFloat ("Speed", Mathf.Abs (PlRb.velocity.x));

	}

	public void TouchLeft(){
		PlRb.velocity = new Vector2 (-maxSpeed, PlRb.velocity.y);
		PlSr.flipX = true;
	}
	public void TouchRight(){
		PlRb.velocity = new Vector2 (maxSpeed, PlRb.velocity.y);
		PlSr.flipX = false;
	}
	public void TouchJump(){
		if (IsGround) {
			PlRb.velocity = new Vector2 (PlRb.velocity.x, JumpForce);
		}
	}
	public void TouchAttack(){
		MyAnim.SetBool ("IsStrike", true);
	}

	void OnTriggerEnter2D(Collider2D other){
		if (other.gameObject.tag == "Ground") {
			IsGround = true;
		}
	}
	void OnTriggerExit2D(Collider2D other){
		if (other.gameObject.tag == "Ground") {
			IsGround = false;
		}
	}

   
}
