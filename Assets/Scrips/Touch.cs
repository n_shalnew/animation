﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Touch : MonoBehaviour {

	private CharacterControllerScript player;


	void Start()
	{
		player = FindObjectOfType<CharacterControllerScript>();
	}

	public void LeftArrow()
	{
		player.moveright = false;
		player.moveleft = true;
	}
	public void RightArrow()
	{
		player.moveright = true;
		player.moveleft = false;
	}
	public void ReleaseLeftArrow()
	{
		player.moveleft = false;
	}
	public void ReleaseRightArrow()
	{
		player.moveright = false;

	}
	public void TouchStrike()
	{
		player.canTouchStrike = true;
	}
	public void ReleaseTouchStrike()
	{
		player.canTouchStrike = false;
	}
}
