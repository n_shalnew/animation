﻿using UnityEngine;
using System.Collections;

public class ForOrc : MonoBehaviour
{

    // Use this for initialization
    public float direction;
    private Vector2 movement;
    private Rigidbody2D rb;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();

        if (direction == 1)
        {
            Vector3 theScale = transform.localScale;
            //зеркально отражаем персонажа по оси Х
            theScale.x *= -1;
            //задаем новый размер персонажа, равный старому, но зеркально отраженный
            transform.localScale = theScale;
        }
    }

    void Update()
    {
        movement = new Vector2(5 * direction, 0);
        rb.velocity = movement;
    }
}
